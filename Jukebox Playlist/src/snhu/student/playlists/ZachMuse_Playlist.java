package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class ZachMuse_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	ArrayList<Song> M83Tracks = new ArrayList<Song>();
    M83 m83Band = new M83();
	
    M83Tracks = m83Band.getM83Songs();
	
	playlist.add(M83Tracks.get(0));
	playlist.add(M83Tracks.get(1));
	playlist.add(M83Tracks.get(2));
	
	
    Grimes grimesBand = new Grimes();
	ArrayList<Song> grimesTracks = new ArrayList<Song>();
    grimesTracks = grimesBand.getGrimesSongs();
	
	playlist.add(grimesTracks.get(0));
	playlist.add(grimesTracks.get(1));
	playlist.add(grimesTracks.get(2));
	

    ImagineDragons idBand = new ImagineDragons();
	ArrayList<Song> idTracks = new ArrayList<Song>();
    idTracks = idBand.getImagineDragonsSongs();
	
	playlist.add(idTracks.get(0));
	playlist.add(idTracks.get(1));
	playlist.add(idTracks.get(2));
	
    return playlist;
	}
}
