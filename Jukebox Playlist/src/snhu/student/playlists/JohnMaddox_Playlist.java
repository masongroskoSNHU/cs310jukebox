package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class JohnMaddox_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	ArrayList<Song> jimiHendrixTracks = new ArrayList<Song>();
    JimiHendrix jimiHendrix = new JimiHendrix();
	
    jimiHendrixTracks = jimiHendrix.getJimiHendrixSongs();
	
	playlist.add(jimiHendrixTracks.get(0));
	playlist.add(jimiHendrixTracks.get(1));
	
    ImagineDragons imagineDragonsBand = new ImagineDragons();
	ArrayList<Song> imagineDragonsTracks = new ArrayList<Song>();
    imagineDragonsTracks = imagineDragonsBand.getImagineDragonsSongs();
	
	playlist.add(imagineDragonsTracks.get(0));
	playlist.add(imagineDragonsTracks.get(1));
	playlist.add(imagineDragonsTracks.get(2));
	
    return playlist;
	}
}
