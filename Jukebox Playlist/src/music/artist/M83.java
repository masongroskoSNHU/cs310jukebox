package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class M83 {

	ArrayList<Song> albumTracks;
    String albumTitle;
    
    //Adding M83
    public M83() {
    }
    
    public ArrayList<Song> getM83Songs() {
    	
    	 albumTracks = new ArrayList<Song>();                           //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Intro", "M83");         				//Create a song
         Song track2 = new Song("Midnight City", "M83");        //Create another song
         Song track3 = new Song("Raconte-Moi Une Histoire", "M83");       //Create a third song
         this.albumTracks.add(track1);                                  //Add the first song to song list
         this.albumTracks.add(track2);                                  //Add the second song to song list
         this.albumTracks.add(track3);                                  //Add the third song to song list
         return albumTracks;                                            //Return the songs for Adele in the form of an ArrayList
    }
}