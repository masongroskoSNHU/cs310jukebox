package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class Grimes {

	ArrayList<Song> albumTracks;
    String albumTitle;
    
    //Adding Grimes
    public Grimes() {
    }
    
    public ArrayList<Song> getGrimesSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                           //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Genesis", "Grimes");         				//Create a song
         Song track2 = new Song("Oblivion", "Grimes");        //Create another song
         Song track3 = new Song("Flesh without Blood", "Grimes");       //Create a third song
         this.albumTracks.add(track1);                                  //Add the first song to song list
         this.albumTracks.add(track2);                                  //Add the second song to song list
         this.albumTracks.add(track3);                                  //Add the third song to song list
         return albumTracks;                                            //Return the songs for Adele in the form of an ArrayList
    }
}